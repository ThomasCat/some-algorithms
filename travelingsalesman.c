#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#define MAX 5
#define INFINITY 9999
int final_path[MAX+1];
bool visited[MAX];
int final_res = INFINITY;
void copyToFinal(int curr_path[]) {
    for (int i=0; i<MAX; i++){
        final_path[i] = curr_path[i]; 
    }
    final_path[MAX] = curr_path[0]; 
}
int firstMin(int adj[MAX][MAX], int i) { 
    int min = INFINITY; 
    for (int k=0; k<MAX; k++) {
        if (adj[i][k]<min && i != k) {
            min = adj[i][k]; 
        }
    }
    return min; 
}
int secondMin(int adj[MAX][MAX], int i) { 
    int first = INFINITY, second = INFINITY; 
    for (int j=0; j<MAX; j++) { 
        if (i == j) {
            continue;
        } 
        if (adj[i][j] <= first) { 
            second = first; 
            first = adj[i][j]; 
        } 
        else if (adj[i][j] <= second && adj[i][j] != first){ 
            second = adj[i][j]; 
        }
    } 
    return second; 
}
void TSPRec(int adj[MAX][MAX], int curr_bound, int curr_weight, int level, int curr_path[]) {
    if (level==MAX) {
        if (adj[curr_path[level-1]][curr_path[0]] != 0) {
            int curr_res = curr_weight + adj[curr_path[level-1]][curr_path[0]];
            if (curr_res < final_res) { 
                copyToFinal(curr_path); 
                final_res = curr_res; 
            } 
        } 
        return; 
    }
    for (int i=0; i<MAX; i++) {
        if (adj[curr_path[level-1]][i] != 0 && visited[i] == false) { 
            int temp = curr_bound; 
            curr_weight += adj[curr_path[level-1]][i];
            if (level==1) {
              curr_bound -= ((firstMin(adj, curr_path[level-1]) + firstMin(adj, i))/2); 
            }else{
              curr_bound -= ((secondMin(adj, curr_path[level-1]) + firstMin(adj, i))/2); 
            }
            if (curr_bound + curr_weight < final_res) { 
                curr_path[level] = i; 
                visited[i] = true;
                TSPRec(adj, curr_bound, curr_weight, level+1, curr_path); 
            }
            curr_weight -= adj[curr_path[level-1]][i]; 
            curr_bound = temp;
            memset(visited, false, sizeof(visited)); 
            for (int j=0; j<=level-1; j++) {
                visited[curr_path[j]] = true;
            } 
        } 
    } 
}
void TSP(int adj[MAX][MAX]) { 
    int curr_path[MAX+1];
    int curr_bound = 0; 
    memset(curr_path, -1, sizeof(curr_path)); 
    memset(visited, 0, sizeof(curr_path));
    for (int i=0; i<MAX; i++) {
        curr_bound += (firstMin(adj, i) + secondMin(adj, i)); 
    }
    curr_bound = (curr_bound&1) ? curr_bound/2 + 1 : curr_bound/2;
    visited[0] = true; 
    curr_path[0] = 0;
    TSPRec(adj, curr_bound, 0, 1, curr_path); 
} 
void generateAdjacencyMatrix(int adj[MAX][MAX]){
	for(int i=0; i<MAX; i++){
		for(int j=0; j<MAX; j++){
			printf(" %d  ", adj[i][j]);
		}
		printf("\n\n");
	}
}
int main() { 
    int adj[MAX][MAX]={NULL};
    for(int i=0; i<MAX; i++){
        for(int j=0; j<MAX; j++){
            if(j==i){
                adj[i][j]=0;
                continue;
            }
            if(adj[i][j]!=NULL){continue;}
            printf("Enter distance from city %d to %d: ", i, j);
            scanf("%d", &adj[i][j]);
            adj[j][i]=adj[i][j];
        }
        printf("\n");
    }
    TSP(adj); 
    printf("Therefore, the adjacency matrix is:\n\n");
	generateAdjacencyMatrix(adj);
    printf("________________________________________\n");
    printf("Minimum cost : %d\n", final_res); 
    printf("Path Taken : "); 
    for (int i=0; i<=MAX; i++) {
        printf("%d", final_path[i]);
        if (i!=MAX) {
            printf(" -> ");
        }
    }
    printf("\n");
    return 0; 
} 






/*

Draw and initializethe root node (see below for details).
2.  Repeat the following step until a solution (i.e., a complete circuit, represented by a terminalnode) has been foundandno unexplored non-terminal node has a smaller bound than thelength of the best solution found:–  Choose an unexplored non-terminal node with the smallest bound, andprocessit (seepage 2 for details about this step).
3.  When a solution has been foundandno unexplored non-terminal node has a smaller boundthan the length of the best solution found, then the best solution found is optimal.

*/



/*


int v, e, w, i, j, G[MAX][MAX];

    printf("\nEnter number of vertices: ");
	scanf("%d", &v);
	printf("Enter number of edges: ");
	scanf("%d", &e);
	printf("\nEnter weight of edges (e.g. from vertex 1 to vertex 2, enter \'7\'):\n\n");
	for (i=1; i<=e; i++){
        for (j=1; j<=e; j++){
            if (i==j){
                G[i][j]=0;
                continue;
            }
            if (G[i][j]!=0){
                continue;
            }
		    printf("Enter weight for %d<->%d: ", i, j);
		    scanf("%d", &w);
            G[i][j]=G[j][i]=w;
        }
	}


    printf("Therefore, the adjacency matrix is:\n\n");
	generateAdjacencyMatrix();

*/