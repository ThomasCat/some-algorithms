#include <bits/stdc++.h>
#include <stdbool.h>
#define MAX 4
struct Node{
    Node* parent;
    int pathCost;
    int cost;
    int workerID;
    int jobID;
    bool assigned[MAX]; 
};
struct comp{ 
    bool operator()(const Node* lhs, const Node* rhs) const{
        return lhs->cost > rhs->cost; 
    } 
};
Node* newNode(int x, int y, bool assigned[], Node* parent){ 
    Node* node = new Node; 
    for (int j = 0; j < MAX; j++){
        node->assigned[j] = assigned[j];
    } 
    node->assigned[y] = true;
    node->parent = parent;
    node->workerID = x; 
    node->jobID = y;
    return node; 
}
int calculateCost(int costMatrix[MAX][MAX], int x, int y, bool assigned[]){
    int cost = 0;
    bool available[MAX] = {true};
    for (int i = x+1; i<MAX; i++){ 
        int min = INT_MAX, minIndex = -1;
        for (int j = 0; j<MAX; j++) {
            if (!assigned[j] && available[j] && costMatrix[i][j] < min){
                minIndex = j;
                min = costMatrix[i][j]; 
            } 
        }
        cost += min;
        available[minIndex] = false; 
    }
    return cost; 
} 
void printAssignments(Node *min){
    if(min->parent==NULL){
        return;
    }
    printAssignments(min->parent);
    printf("Assign worker %d to job %d\n", min->workerID, min->jobID);
}
int findMinCost(int costMatrix[MAX][MAX]){
    std::priority_queue<Node*, std::vector<Node*>, comp> pq;
    bool assigned[MAX] = {false}; 
    Node* root = newNode(-1, -1, assigned, NULL); 
    root->pathCost = root->cost = 0; 
    root->workerID = -1;
    pq.push(root);
    while (!pq.empty()){
      Node* min = pq.top();
      pq.pop();
      int i = min->workerID + 1;
      if (i == MAX){ 
          printAssignments(min); 
          return min->cost; 
      }
      for (int j = 0; j < MAX; j++){
        if (!min->assigned[j]){
          Node* child = newNode(i, j, min->assigned, min);
          child->pathCost = min->pathCost + costMatrix[i][j];
          child->cost = child->pathCost + calculateCost(costMatrix, i, j, child->assigned);
          pq.push(child); 
        } 
      } 
    } 
}
void generateAdjacencyMatrix(int adj[MAX][MAX]){
	for(int i=0; i<MAX; i++){
		for(int j=0; j<MAX; j++){
			printf(" %d  ", adj[i][j]);
		}
		printf("\n\n");
	}
}
int main(){
    int adj[MAX][MAX];
    for(int i=0; i<MAX; i++){
        printf("Enter jobs for worker %d: \n", i);
        for(int j=0; j<MAX; j++){
            printf("Cost for job #%d: ", j);
            scanf("%d", &adj[i][j]);
        }
        printf("\n\n");
    }
    printf("Therefore, the adjacency matrix is:\n\n");
	generateAdjacencyMatrix(adj);
    printf("\nOptimal Cost is %d", findMinCost(adj)); 
    printf("\n");
    return 0; 
}