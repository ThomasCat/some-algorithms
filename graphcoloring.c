#include<stdio.h>
#define MAX 10

int G[MAX][MAX], m, edges, v1, v2, i, j, vertices, a, b, coloring_index[MAX];
char colors[MAX][6]={"Black", "White", "Red", "Blue", "Green", "Yellow", "Orange", "Pink", "Gray"};
void generateColors(int, int);
void colorGraph(int, int);
void generateAdjacencyMatrix();
int main(){
	printf("\nEnter number of vertices: ");
	scanf("%d", &vertices);
	printf("Enter number of edges: ");
	scanf("%d", &edges);
	m=vertices-1;
	printf("\nEnter undirected edges of graph (e.g. from vertex 1 to vertex 2, enter \'1 2\'):\n\n");
	for (i=1; i<=edges; i++){
		printf("Enter edge #%d: ", i);
		scanf("%d %d", &v1, &v2);
		G[v1][v2] = G[v2][v1] = 1;
	}
	printf("-----------------------\n");
	colorGraph(1, vertices);

	printf("Therefore, the adjacency matrix is:\n\n");
	generateAdjacencyMatrix();
	printf("-----------------------");
	for(i=1; i<=vertices; i++){
		printf("\nVertex %d should be %s", i, colors[coloring_index[i]]);
	}
	printf("\n");
 	return 0;
}
void generateColors(int k, int vertices){
	while(1){
		a=coloring_index[k]+1;
		b=m+1;
		coloring_index[k] = a%b; 
		if(coloring_index[k]==0){
			return;
		}
		for(j=1; j<=vertices; j++){
			if(G[k][j] && coloring_index[k] == coloring_index[j]){
				break;
			}
		}
		if(j==vertices+1){
			return;
		}
	}
}
void colorGraph(int k, int vertices){
	generateColors(k, vertices);
	if(coloring_index[k] ==  0){
		return;
	}
	if(k == vertices){
		return;
	}else{
		colorGraph(k+1, vertices);
	}
}
void generateAdjacencyMatrix(){
	for(i=1; i<vertices+1; i++){
		for(j=1; j<vertices+1; j++){
			printf(" %d   ", G[i][j]);
		}
		printf("\n\n");
	}
}